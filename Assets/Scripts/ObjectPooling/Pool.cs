﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ObjectPooling
{
    public abstract class Pool<TPoolObject> : MonoBehaviour, IPool<TPoolObject>
        where TPoolObject : MonoBehaviour
    {
        private IFactory<TPoolObject> _factory;
        private readonly List<TPoolObject> _poolObjects = new List<TPoolObject>();

        private void Awake()
        {
            _factory = GetComponent<IFactory<TPoolObject>>();
            Debug.Assert(_factory != null, $"{nameof(TPoolObject)} is null!");
        }

        public TPoolObject Request()
        {
            foreach (var obj in _poolObjects.Where(obj => !obj.gameObject.activeInHierarchy))
            {
                return obj;
            }

            TPoolObject poolObject = _factory.Create();
            _poolObjects.Add(poolObject);
            return poolObject;
        }

        public void Return(TPoolObject poolObject)
        {
            poolObject.gameObject.SetActive(false);
        }
    }
}
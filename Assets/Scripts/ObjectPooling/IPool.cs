﻿namespace ObjectPooling
{
    public interface IPool<TPoolObject>
    {
        TPoolObject Request();
        void Return(TPoolObject poolObject);
    }
}
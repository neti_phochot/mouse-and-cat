﻿namespace ObjectPooling
{
    public interface IFactory<out TPoolObject>
    {
        TPoolObject Create();
    }
}
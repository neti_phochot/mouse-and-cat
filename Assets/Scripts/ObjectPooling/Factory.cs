using UnityEngine;

namespace ObjectPooling
{
    public abstract class Factory<TPoolObject> : MonoBehaviour, IFactory<TPoolObject>
    {
        public abstract TPoolObject Create();
    }
}
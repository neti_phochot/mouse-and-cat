﻿using UnityEngine;

namespace Waypoint.Visual
{
    public class DragPoint : MonoBehaviour, ILocation
    {
        public Vector3 GetLocation() => transform.position;
        public void SetLocation(Vector3 location) => transform.position = location;
    }
}
﻿using ObjectPooling;
using UnityEngine;

namespace Waypoint.Visual
{
    public class DragPointFactory : Factory<DragPoint>
    {
        [SerializeField] private DragPoint dragPointPrefab;
        public override DragPoint Create() => Instantiate(dragPointPrefab, transform);
    }
}
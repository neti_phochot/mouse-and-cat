﻿using Core;
using UnityEngine;

namespace Waypoint.Visual
{
    public class Pointer : MonoBehaviour, ILocation, IVisible
    {
        public Vector3 GetLocation() => transform.position;
        public void SetLocation(Vector3 location) => transform.position = location;

        public bool Visible
        {
            get => gameObject.activeInHierarchy;
            set => gameObject.SetActive(value);
        }
    }
}
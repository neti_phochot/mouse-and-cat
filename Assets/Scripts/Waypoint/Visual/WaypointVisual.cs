﻿using System.Collections.Generic;
using UnityEngine;

namespace Waypoint.Visual
{
    public class WaypointVisual : MonoBehaviour
    {
        [SerializeField] private WaypointManager waypointManager;
        [SerializeField] private Pointer pointer;

        [Header("DRAG POINT")] [SerializeField]
        private DragPointPool dragPointPool;

        private readonly Dictionary<Waypoint, DragPoint> _dragPoints = new Dictionary<Waypoint, DragPoint>();

        private void Awake()
        {
            pointer.Visible = false;
        }

        private void OnEnable()
        {
            waypointManager.OnWaypointUpdated += OnWaypointUpdatedEvent;
        }

        private void OnDisable()
        {
            waypointManager.OnWaypointUpdated -= OnWaypointUpdatedEvent;
        }

        private void OnWaypointUpdatedEvent(
            IReadOnlyList<Waypoint> waypoints, Waypoint waypoint,
            WaypointManager.WaypointOperation waypointOperation)
        {
            switch (waypointOperation)
            {
                case WaypointManager.WaypointOperation.Add:
                    DragPoint dragPoint = dragPointPool.Request();
                    dragPoint.SetLocation(waypoint.GetLocation());
                    dragPoint.gameObject.SetActive(true);
                    _dragPoints.Add(waypoint, dragPoint);

                    pointer.Visible = true;
                    pointer.SetLocation(waypoint.GetLocation());

                    break;

                case WaypointManager.WaypointOperation.Remove:
                    dragPointPool.Return(_dragPoints[waypoint]);
                    _dragPoints.Remove(waypoint);
                    break;

                case WaypointManager.WaypointOperation.Clear:
                    foreach (var dp in _dragPoints.Values)
                    {
                        dragPointPool.Return(dp);
                    }

                    pointer.Visible = false;
                    _dragPoints.Clear();
                    break;
            }
        }
    }
}
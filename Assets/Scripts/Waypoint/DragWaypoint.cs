﻿using System;
using Core.Player;
using UnityEngine;
using UnityEngine.AI;

namespace Waypoint
{
    public class DragWaypoint : MonoBehaviour
    {
        [SerializeField] private string[] navMeshGroundlayers;
        [Header("PLAYER")] [SerializeField] private PlayerInput playerInput;
        [SerializeField] private Transform playerTransform;
        [Header("DRAG")] [SerializeField] private float startDragMaxDist;
        [SerializeField] private LayerMask whatIsGround;
        [SerializeField] private Camera cam;
        [Header("WAYPOINT")] [SerializeField] private WaypointManager waypointManager;
        [SerializeField] private float waypointPadding;

        public event Action OnDragStart;
        public event Action OnDrag;
        public event Action OnDragEnd;

        private bool _isDragging;

        private void OnEnable()
        {
            CancelInvoke(nameof(playerInput));
            Invoke(nameof(InputDelay), 0.2f);
        }

        private void InputDelay()
        {
            playerInput.OnInputAction += OnInputActionEvent;
            waypointManager.ClearWaypoint();
        }

        private void OnDisable()
        {
            playerInput.OnInputAction -= OnInputActionEvent;
            waypointManager.ClearWaypoint();
        }

        private void Update()
        {
            Drag();
        }

        #region INPUT

        private void OnInputActionEvent(InputActionType inputActionType)
        {
            switch (inputActionType)
            {
                case InputActionType.LeftMouseDown:
                    DragStart();
                    break;
                case InputActionType.LeftMouseUp:
                    DragEnd();
                    break;
                case InputActionType.RightMouseDown:
                    SetWaypoint();
                    break;
            }
        }

        #endregion

        #region DRAG

        private void DragStart()
        {
            if (_isDragging) return;

            (bool isHit, RaycastHit raycastHit) = GetDragRaycast();
            if (!isHit || !ClickedInPlayerRange(raycastHit.point)) return;

            waypointManager.ClearWaypoint();
            _isDragging = true;
        }

        private void Drag()
        {
            if (!_isDragging) return;

            (bool isHit, RaycastHit raycastHit) = GetDragRaycast();
            if (!isHit) return;
            if (!IsWalkable(raycastHit.point))
            {
                DragEnd();
                return;
            }

            if (!waypointManager.HasWaypoint())
            {
                //DRAG START
                waypointManager.AddWaypoint(new Waypoint(raycastHit.point));
                OnDragStart?.Invoke();
                return;
            }

            //DRAG
            Waypoint lastWaypoint = waypointManager.GetWaypoints()[waypointManager.GetWaypoints().Count - 1];
            if (Vector3.Distance(lastWaypoint.GetLocation(), raycastHit.point) < waypointPadding) return;
            waypointManager.AddWaypoint(new Waypoint(raycastHit.point));
            OnDrag?.Invoke();
        }


        private void SetWaypoint()
        {
            (bool isHit, RaycastHit raycastHit) = GetDragRaycast();
            if (!isHit) return;
            if (!IsWalkable(raycastHit.point))
            {
                return;
            }

            waypointManager.ClearWaypoint();
            waypointManager.AddWaypoint(new Waypoint(raycastHit.point));
        }

        private void DragEnd()
        {
            if (!_isDragging) return;
            //DRAG END
            _isDragging = false;
            OnDragEnd?.Invoke();
        }

        #endregion

        private bool IsWalkable(Vector3 location)
        {
            int layer = 0;
            foreach (var groundlayer in navMeshGroundlayers)
            {
                layer |= 1 << NavMesh.GetAreaFromName(groundlayer);
            }

            return NavMesh.SamplePosition(location, out NavMeshHit hit, 0.39f, layer);
        }

        private bool ClickedInPlayerRange(Vector3 clickedLocation)
        {
            return Vector3.Distance(clickedLocation, playerTransform.transform.position) < startDragMaxDist;
        }

        private (bool isHit, RaycastHit raycastHit) GetDragRaycast()
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            bool isHit = Physics.Raycast(ray, out RaycastHit raycastHit, Mathf.Infinity, whatIsGround);
            return (isHit, raycastHit);
        }
    }
}
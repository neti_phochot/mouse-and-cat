﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Waypoint
{
    public class WaypointManager : MonoBehaviour
    {
        public enum WaypointOperation
        {
            Add,
            Remove,
            Clear,
        }

        public event Action<IReadOnlyList<Waypoint>, Waypoint, WaypointOperation> OnWaypointUpdated;

        private readonly List<Waypoint> _waypoints = new List<Waypoint>();
        public List<Waypoint> GetWaypoints() => _waypoints;

        public bool HasWaypoint() => _waypoints.Count > 0;

        public void AddWaypoint(Waypoint waypoint)
        {
            _waypoints.Add(waypoint);
            OnWaypointUpdated?.Invoke(_waypoints, waypoint, WaypointOperation.Add);
        }

        public void RemoveWaypoint(Waypoint waypoint)
        {
            OnWaypointUpdated?.Invoke(_waypoints, waypoint, WaypointOperation.Remove);
            _waypoints.Remove(waypoint);
        }

        public void ClearWaypoint()
        {
            _waypoints.Clear();
            OnWaypointUpdated?.Invoke(_waypoints, default, WaypointOperation.Clear);
        }
    }
}
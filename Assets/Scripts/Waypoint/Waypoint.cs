using UnityEngine;

namespace Waypoint
{
    public struct Waypoint : ILocation
    {
        private Vector3 _location;

        public Waypoint(Vector3 location)
        {
            _location = location;
        }

        public Vector3 GetLocation() => _location;
        public void SetLocation(Vector3 location) => _location = location;
    }
}
﻿using UnityEngine;

public interface ILocation
{
    Vector3 GetLocation();
    void SetLocation(Vector3 location);
}
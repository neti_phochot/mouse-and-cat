﻿using UnityEngine;

namespace Utils
{
    public class LookAtCamera : MonoBehaviour
    {
        private Camera _cam;

        private void Awake()
        {
            _cam = Camera.main;
        }

        private void FixedUpdate()
        {
            transform.rotation = Quaternion.LookRotation(_cam.transform.forward);
        }
    }
}
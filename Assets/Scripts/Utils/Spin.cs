﻿using DG.Tweening;
using UnityEngine;

namespace Utils
{
    public class Spin : MonoBehaviour
    {
        [SerializeField] private Transform spinTransform;
        [SerializeField] private float spinDuration;
        [SerializeField] private Vector3 axis;
        [SerializeField] private Ease ease;

        private void Awake()
        {
            SetAnime(spinDuration);
        }

        private void SetAnime(float duration)
        {
            Transform buttonTransform = spinTransform.transform;
            DOTween.Kill(buttonTransform);
            buttonTransform.localRotation = Quaternion.identity;
            buttonTransform.DOLocalRotate(axis, duration)
                .SetEase(ease)
                .SetLoops(-1, LoopType.Restart);
        }
    }
}
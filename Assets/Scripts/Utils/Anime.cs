﻿using DG.Tweening;
using UnityEngine;

namespace Utils
{
    public class Anime : MonoBehaviour
    {
        [Header("ANIMATION")] [SerializeField] private float animeDuration = 2f;
        [SerializeField] private Vector3 animeScale = new Vector3(1.2f, 0.8f, 1f);
        [SerializeField] private Ease ease = Ease.InBounce;

        private void OnEnable()
        {
            SetAnime(animeDuration);
        }

        private void SetAnime(float duration)
        {
            DOTween.Kill(transform);
            transform.localScale = Vector3.one;
            transform.DOScale(animeScale, duration)
                .SetEase(ease)
                .SetLoops(-1, LoopType.Yoyo);
        }
    }
}
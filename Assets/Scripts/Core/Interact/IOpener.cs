﻿namespace Core.Interact
{
    public interface IOpener
    {
        void Open();
        void Close();
    }
}
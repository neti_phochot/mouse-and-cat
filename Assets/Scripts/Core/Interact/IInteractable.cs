﻿namespace Core.Interact
{
    public interface IInteractable
    {
        void Interact(IClicker clicker);
    }
}
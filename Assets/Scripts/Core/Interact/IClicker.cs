﻿using Core.Entity;
using UnityEngine;

namespace Core.Interact
{
    public interface IClicker
    {
        Transform GetClicker();
    }
}
﻿using UnityEngine;
using Waypoint;

namespace Core.Player
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private WaypointManager waypointManager;
        [SerializeField] private float reactDistance;
        private Entity.Player _player;

        private void Awake()
        {
            _player = GetComponent<Entity.Player>();
        }

        private void FixedUpdate()
        {
            Move();
        }

        private void Move()
        {
            if (!waypointManager.HasWaypoint()) return;

            Waypoint.Waypoint waypoint = waypointManager.GetWaypoints()[0];
            _player.GetAi().SetDestination(waypoint.GetLocation());

            if (Vector3.Distance(waypoint.GetLocation(), _player.GetLocation()) > reactDistance) return;
            waypointManager.RemoveWaypoint(waypoint);
        }
    }
}
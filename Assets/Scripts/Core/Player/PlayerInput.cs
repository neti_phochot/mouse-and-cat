﻿using System;
using UnityEngine;

namespace Core.Player
{
    public enum InputActionType
    {
        LeftMouseDown,
        LeftMouseUp,
        RightMouseDown,
        RightMouseUp,
    }

    public class PlayerInput : MonoBehaviour
    {
        public event Action<InputActionType> OnInputAction;

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                OnInputAction?.Invoke(InputActionType.LeftMouseDown);
            }
            else if (Input.GetMouseButtonUp(0))
            {
                OnInputAction?.Invoke(InputActionType.LeftMouseUp);
            }
            else if (Input.GetMouseButtonDown(1))
            {
                OnInputAction?.Invoke(InputActionType.RightMouseDown);
            }
            else if (Input.GetMouseButtonUp(1))
            {
                OnInputAction?.Invoke(InputActionType.RightMouseUp);
            }
        }
    }
}
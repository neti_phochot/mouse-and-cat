﻿namespace Core
{
    public interface IVisible
    {
        bool Visible { get; set; }
    }
}
﻿using Core.Interact;
using Game.Portal;
using UnityEngine;
using UnityEngine.AI;

namespace Core.Entity
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class Player : MonoBehaviour, ILivingEntity, ITeleporter, IClicker
    {
        private NavMeshAgent _agent;
        private float _walkSpeedMultiplier = 1f;
        private float _agentSpeed;

        private void Awake()
        {
            _agent = GetComponent<NavMeshAgent>();

            _agentSpeed = _agent.speed;
        }

        public Vector3 GetLocation() => transform.position;
        public void SetLocation(Vector3 location) => transform.position = location;

        public void Teleport(Vector3 location, Quaternion quaternion)
        {
            SetLocation(location);
            transform.rotation = quaternion;
        }

        public bool Visible
        {
            get => gameObject.activeInHierarchy;
            set => gameObject.SetActive(value);
        }

        public NavMeshAgent GetAi() => _agent;
        public Transform GetClicker() => transform;

        public void AddVelocity(Vector3 velocity)
        {
            //Nothing
        }

        public void SetWalkSpeed(float speed)
        {
            _walkSpeedMultiplier = speed;
            _agent.speed = _agentSpeed * _walkSpeedMultiplier;
        }

        public float GetWalkSpeed() => _walkSpeedMultiplier;
    }
}
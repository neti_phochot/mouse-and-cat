﻿using System;
using UnityEngine;
using UnityEngine.AI;

namespace Core.Entity
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class Cat : MonoBehaviour, ILivingEntity
    {
        [SerializeField] private float attackDelay = 0.5f;

        private NavMeshAgent _agent;
        private Rigidbody _rigidbody;
        private float _walkSpeedMultiplier = 1f;
        private float _agentSpeed;
        public event Action<ILivingEntity> OnHitLivingEntity;

        private void Awake()
        {
            _agent = GetComponent<NavMeshAgent>();
            _rigidbody = GetComponent<Rigidbody>();

            _agentSpeed = _agent.speed;
        }

        public Vector3 GetLocation() => transform.position;
        public void SetLocation(Vector3 location) => transform.position = location;


        public bool Visible
        {
            get => gameObject.activeInHierarchy;
            set => gameObject.SetActive(value);
        }

        public NavMeshAgent GetAi() => _agent;

        #region TEST

        private Vector3 _velocity;

        public void AddVelocity(Vector3 velocity)
        {
            _velocity = velocity;

            _agent.isStopped = true;
            CancelInvoke(nameof(VelocityDelay));
            Invoke(nameof(VelocityDelay), attackDelay);
        }

        private void VelocityDelay()
        {
            _rigidbody.AddForce(_velocity, ForceMode.Impulse);
            CancelInvoke(nameof(ActiveAgentDelay));
            Invoke(nameof(ActiveAgentDelay), 1);
        }

        private void ActiveAgentDelay()
        {
            _agent.isStopped = false;
            _rigidbody.velocity = Vector3.zero;
            _rigidbody.angularVelocity = Vector3.zero;
        }

        #endregion

        private void OnCollisionEnter(Collision collision)
        {
            if (!collision.collider.TryGetComponent(out ILivingEntity livingEntity)) return;
            OnHitLivingEntity?.Invoke(livingEntity);
        }

        public void SetWalkSpeed(float speed)
        {
            _walkSpeedMultiplier = speed;
            _agent.speed = _agentSpeed * _walkSpeedMultiplier;
        }

        public float GetWalkSpeed() => _walkSpeedMultiplier;
    }
}
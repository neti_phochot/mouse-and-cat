﻿namespace Core.Entity
{
    public interface ILivingEntity : IEntity, IAI, IVelocity
    {
        void SetWalkSpeed(float speed);
        float GetWalkSpeed();
    }
}
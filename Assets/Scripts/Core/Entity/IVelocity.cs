﻿using UnityEngine;

namespace Core.Entity
{
    public interface IVelocity
    {
        void AddVelocity(Vector3 velocity);
    }
}
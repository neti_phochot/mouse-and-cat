﻿using UnityEngine.AI;

namespace Core.Entity
{
    public interface IAI
    {
        NavMeshAgent GetAi();
    }
}
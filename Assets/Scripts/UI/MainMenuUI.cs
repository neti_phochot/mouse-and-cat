﻿using DG.Tweening;
using UI.Dialogue;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    public class MainMenuUI : MonoBehaviour
    {
        [SerializeField] private Button playButton;
        [Header("ANIMATION")] [SerializeField] private float animeDuration;
        [SerializeField] private Vector3 animeScale;
        [SerializeField] private Ease ease;

        [Header("TUTORIAL")] [SerializeField] private Button tutorialButton;
        [SerializeField] private DialogueUI dialogueUI;

        private void Awake()
        {
            playButton.onClick.AddListener(Play);
            tutorialButton.onClick.AddListener(dialogueUI.StartDialogue);
            SetAnime(animeDuration);
        }

        private void Play()
        {
            SceneManager.LoadScene(1);
        }

        private void SetAnime(float duration)
        {
            Transform buttonTransform = playButton.transform;
            DOTween.Kill(buttonTransform);
            buttonTransform.localScale = Vector3.one;
            buttonTransform.DOScale(animeScale, duration)
                .SetEase(ease)
                .SetLoops(-1, LoopType.Yoyo);
        }
    }
}
﻿using System;
using Game;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class GameUI : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private TextMeshProUGUI scoreText;
        [SerializeField] private TextMeshProUGUI timerText;


        [Header("GAME")] [SerializeField] private StartGameUI startGameUI;

        [Header("GAME RESULT")] [SerializeField]
        private GameOverUI gameOverUI;

        [SerializeField] private WinUI winUI;
        [Header("PAUSED UI")] [SerializeField] private Button pauseButton;
        [SerializeField] private PauseGameUI pauseGameUI;

        private void Awake()
        {
            pauseButton.onClick.AddListener(pauseGameUI.Pause);
            startGameUI.SetStartAction(gameManager.StartGame);
            startGameUI.Visible = true;
        }

        private void OnEnable()
        {
            gameManager.OnScoreChanged += OnScoreChangedEvent;
            gameManager.OnTimeTick += OnTimeTickEvent;
            gameManager.OnGameOver += OnGameOverEvent;
        }


        private void OnDisable()
        {
            gameManager.OnScoreChanged -= OnScoreChangedEvent;
            gameManager.OnTimeTick -= OnTimeTickEvent;
            gameManager.OnGameOver -= OnGameOverEvent;
        }

        private void SetScore(int score)
        {
            scoreText.text = $"{score}";
        }

        private void SetTimer(int timer)
        {
            TimeSpan timeSpan = TimeSpan.FromSeconds(timer);
            timerText.text = $"{(int)timeSpan.TotalMinutes}:{timeSpan.Seconds:00}";
            timerText.color = timer > 10 ? Color.white : Color.red;
        }

        private void OnTimeTickEvent(int timer)
        {
            SetTimer(timer);
        }

        private void OnScoreChangedEvent(int oldScore, int score)
        {
            SetScore(score);
        }

        private void OnGameOverEvent(bool win)
        {
            if (win) winUI.SetWin();
            else gameOverUI.SetGameOver();
        }
    }
}
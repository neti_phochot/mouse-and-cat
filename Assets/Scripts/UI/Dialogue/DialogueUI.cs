using System;
using Core;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Dialogue
{
    public class DialogueUI : MonoBehaviour, IVisible
    {
        [Serializable]
        public struct DialogueData
        {
            public Sprite cover;
            [TextArea(3, 10)] public string sentence;
        }

        [Header("UI")] [SerializeField] Image coverImage;
        [SerializeField] TextMeshProUGUI dialogueText;
        [SerializeField] TextMeshProUGUI pageText;
        [SerializeField] private Button nextButton;
        [SerializeField] private Button closeButton;
        [Header("ANIMATION")] [SerializeField] private Transform animeTransform;
        [SerializeField] private float animeDuration = 2f;
        [SerializeField] private Vector3 animeScale = new Vector3(1.2f, 0.8f, 1f);
        [SerializeField] private Ease ease = Ease.InBounce;
        [Header("DIALOGUE")] [SerializeField] private DialogueData[] dialogues;

        private int _dialogueIndex;

        private void Awake()
        {
            nextButton.onClick.AddListener(DisplayNextSentence);
            closeButton.onClick.AddListener(() => { Visible = false; });
        }

        public void StartDialogue()
        {
            Visible = true;
            _dialogueIndex = 0;
            DisplayNextSentence();
        }

        public void DisplayNextSentence()
        {
            int index = _dialogueIndex++ % dialogues.Length;
            pageText.text = $"{index + 1}/{dialogues.Length}";
            DialogueData dialogue = dialogues[index];
            dialogueText.text = dialogue.sentence;
            coverImage.sprite = dialogue.cover;

            SetAnime(animeDuration);
        }

        public bool Visible
        {
            get => gameObject.activeInHierarchy;
            set => gameObject.SetActive(value);
        }

        #region ANIMATION

        private void SetAnime(float duration)
        {
            DOTween.Kill(animeTransform);
            animeTransform.localScale = animeScale;
            animeTransform.DOScale(Vector3.one, duration)
                .SetEase(ease);
        }

        #endregion
    }
}
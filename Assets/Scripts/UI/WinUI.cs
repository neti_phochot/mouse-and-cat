﻿using Core;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    public class WinUI : MonoBehaviour, IVisible
    {
        [SerializeField] private Button restartButton;
        [SerializeField] private Button leaveButton;

        private void Awake()
        {
            leaveButton.onClick.AddListener(MainMenu);
            restartButton.onClick.AddListener(Restart);
        }

        private void Restart()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        public void SetWin()
        {
            Visible = true;
        }

        private void MainMenu()
        {
            SceneManager.LoadScene(0);
        }

        public bool Visible
        {
            get => gameObject.activeInHierarchy;
            set => gameObject.SetActive(value);
        }
    }
}
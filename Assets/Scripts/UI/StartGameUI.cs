﻿using System;
using Core;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class StartGameUI : MonoBehaviour, IVisible
    {
        [SerializeField] private Button startGameButton;

        public void SetStartAction(Action action)
        {
            startGameButton.onClick.AddListener(() =>
            {
                action();
                Visible = false;
            });
        }

        public bool Visible
        {
            get => gameObject.activeInHierarchy;
            set => gameObject.SetActive(value);
        }
    }
}
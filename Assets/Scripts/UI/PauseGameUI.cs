﻿using Core;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI
{
    public class PauseGameUI : MonoBehaviour, IVisible
    {
        [SerializeField] private Button continueButton;
        [SerializeField] private Button restartButton;
        [SerializeField] private Button leaveButton;

        private void Awake()
        {
            continueButton.onClick.AddListener(Continue);
            leaveButton.onClick.AddListener(MainMenu);
            restartButton.onClick.AddListener(Restart);
        }

        private void Restart()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        private void Continue()
        {
            Time.timeScale = 1;
            Visible = false;
        }

        private void MainMenu()
        {
            Time.timeScale = 1;
            SceneManager.LoadScene(0);
        }

        public void Pause()
        {
            Time.timeScale = 0;
            Visible = true;
        }

        public bool Visible
        {
            get => gameObject.activeInHierarchy;
            set => gameObject.SetActive(value);
        }
    }
}
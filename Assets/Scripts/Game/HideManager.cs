﻿using State;
using UnityEngine;

namespace Game
{
    public class HideManager : MonoBehaviour
    {
        [HideInInspector] public Obstacle[] hides;

        private void Awake()
        {
            hides = FindObjectsOfType<Obstacle>();
        }
    }
}
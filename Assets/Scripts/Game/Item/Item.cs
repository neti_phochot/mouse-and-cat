﻿using System;
using Core.Interact;
using UnityEngine;

namespace Game.Item
{
    public abstract class Item : MonoBehaviour, IInteractable
    {
        public event Action<Item> OnPickUpItem;

        public virtual void Interact(IClicker clicker)
        {
            OnPickUpItem?.Invoke(this);
        }
    }
}
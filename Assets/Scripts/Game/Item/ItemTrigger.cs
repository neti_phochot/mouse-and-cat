﻿using Core.Interact;
using UnityEngine;

namespace Game.Item
{
    [RequireComponent(typeof(Item))]
    public class ItemTrigger : MonoBehaviour
    {
        private Item _item;

        private void Awake()
        {
            _item = GetComponent<Item>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.TryGetComponent(out IClicker clicker)) return;
            _item.Interact(clicker);
        }
    }
}
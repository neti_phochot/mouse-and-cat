﻿using Core.Interact;
using UnityEngine;

namespace Game.Item
{
    public class CheeseItem : Item
    {
        [SerializeField] private int score;
        public int GetScore() => score;
        public override void Interact(IClicker clicker)
        {
            base.Interact(clicker);
            Destroy(gameObject);
        }
    }
}
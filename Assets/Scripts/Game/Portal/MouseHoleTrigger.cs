﻿using UnityEngine;

namespace Game.Portal
{
    public class MouseHoleTrigger : MonoBehaviour
    {
        private IPortal _mouseHole;

        private void Awake()
        {
            _mouseHole = GetComponent<IPortal>();
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (!collision.collider.TryGetComponent(out ITeleporter teleporter)) return;
            _mouseHole.EnterPortal(teleporter);
        }
    }
}
﻿using Core.Entity;
using Core.Interact;
using Core.Player;
using UnityEngine;

namespace Game.Portal
{
    public class MouseHoleInteract : MonoBehaviour, IInteractable
    {
        private IPortal _portal;
        public bool Active { get; set; }

        private void Awake()
        {
            _portal = GetComponent<IPortal>();
        }

        public void Interact(IClicker clicker)
        {
            if (!Active) return;
            if (!clicker.GetClicker().TryGetComponent(out ITeleporter teleporter)) return;
            teleporter.Teleport(_portal.GetTeleportLocation(), _portal.GetTeleportRotation());
            teleporter.Visible = true;

            foreach (var portal in _portal.GetLinkedPortals())
            {
                MouseHole mouseHole = (MouseHole)portal;
                mouseHole.SetHoleStatus(MouseHole.HoleStatus.Close);
            }
        }

        private void OnMouseDown()
        {
            //TODO: 
            Interact(FindObjectOfType<Player>(true));
        }
    }
}
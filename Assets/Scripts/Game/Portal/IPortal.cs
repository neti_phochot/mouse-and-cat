﻿using System.Collections.Generic;
using UnityEngine;

namespace Game.Portal
{
    public interface IPortal : ILocation
    {
        List<IPortal> GetLinkedPortals();
        Vector3 GetTeleportLocation();
        Quaternion GetTeleportRotation();
        void EnterPortal(ITeleporter teleporter);
    }
}
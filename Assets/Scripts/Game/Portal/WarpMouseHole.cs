﻿using UnityEngine;

namespace Game.Portal
{
    [RequireComponent(typeof(MouseHoleInteract))]
    public class WarpMouseHole : MouseHole
    {
        private MouseHoleInteract _mouseHoleInteract;

        public override void EnterPortal(ITeleporter teleporter)
        {
            base.EnterPortal(teleporter);

            foreach (var portal in GetLinkedPortals())
            {
                MouseHole mouseHole = (MouseHole)portal;
                mouseHole.SetHoleStatus(HoleStatus.Open);
            }
        }

        protected override void Awake()
        {
            base.Awake();
            _mouseHoleInteract = GetComponent<MouseHoleInteract>();
        }

        public override void SetHoleStatus(HoleStatus status)
        {
            base.SetHoleStatus(status);
            _mouseHoleInteract.Active = status == HoleStatus.Open;
        }
    }
}
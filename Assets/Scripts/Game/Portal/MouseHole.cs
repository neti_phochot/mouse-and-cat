﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Portal
{
    public abstract class MouseHole : MonoBehaviour, IPortal
    {
        public event Action OnEnterPortal;
        public event Action<HoleStatus> OnHoleStatusChanged;

        public enum HoleStatus
        {
            Open,
            Close,
        }

        [SerializeField] private Transform teleportTransform;
        [SerializeField] private MouseHole[] linedMouseHoles;

        private readonly List<IPortal> _linkedPortals = new List<IPortal>();

        private HoleStatus _holeStatus;

        protected virtual void Awake()
        {
            InitPortals();
        }

        private void Start()
        {
            SetHoleStatus(HoleStatus.Close);
        }

        private void InitPortals()
        {
            foreach (var mouseHole in linedMouseHoles)
            {
                _linkedPortals.Add(mouseHole);
            }
        }

        public List<IPortal> GetLinkedPortals() => _linkedPortals;
        public Vector3 GetTeleportLocation() => teleportTransform.position;
        public Quaternion GetTeleportRotation() => teleportTransform.rotation;

        public virtual void EnterPortal(ITeleporter teleporter)
        {
            OnEnterPortal?.Invoke();
            teleporter.Visible = false;
        }

        public virtual void SetHoleStatus(HoleStatus status)
        {
            _holeStatus = status;
            OnHoleStatusChanged?.Invoke(status);
        }

        public Vector3 GetLocation() => transform.position;
        public void SetLocation(Vector3 location) => transform.position = location;
    }
}
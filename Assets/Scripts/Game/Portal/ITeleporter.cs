﻿using Core;
using UnityEngine;

namespace Game.Portal
{
    public interface ITeleporter : IVisible
    {
        void Teleport(Vector3 location, Quaternion quaternion);
    }
}
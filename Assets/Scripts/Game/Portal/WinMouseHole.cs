﻿using UnityEngine;

namespace Game.Portal
{
    public class WinMouseHole : MouseHole
    {
        private Collider _collider;

        protected override void Awake()
        {
            base.Awake();
            _collider = GetComponent<Collider>();
        }

        public override void EnterPortal(ITeleporter teleporter)
        {
            base.EnterPortal(teleporter);
            Debug.Log("YOU WIN!");
        }

        public override void SetHoleStatus(HoleStatus status)
        {
            base.SetHoleStatus(status);
            _collider.enabled = status == HoleStatus.Open;
        }
    }
}
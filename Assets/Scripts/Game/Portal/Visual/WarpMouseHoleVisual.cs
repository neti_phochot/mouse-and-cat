﻿using System;
using Core.Interact;
using UnityEngine;

namespace Game.Portal.Visual
{
    [RequireComponent(typeof(MouseHole))]
    public class WarpMouseHoleVisual : MonoBehaviour
    {
        private IOpener _opener;

        private void Awake()
        {
            _opener = GetComponent<IOpener>();
        }

        private void OnEnable()
        {
            GetComponent<MouseHole>().OnHoleStatusChanged += OnHoleStatusChangedEvent;
        }

        private void OnDisable()
        {
            GetComponent<MouseHole>().OnHoleStatusChanged -= OnHoleStatusChangedEvent;
        }

        private void OnHoleStatusChangedEvent(MouseHole.HoleStatus holeStatus)
        {
            switch (holeStatus)
            {
                case MouseHole.HoleStatus.Open:
                    _opener.Open();
                    break;
                case MouseHole.HoleStatus.Close:
                    _opener.Close();
                    break;
            }
        }
    }
}
﻿using Core.Interact;
using DG.Tweening;
using UnityEngine;

namespace Game.Portal.Visual
{
    public class WinMouseHoleVisualOpener : MonoBehaviour, IOpener
    {
        [Header("VISUAL")] [SerializeField] private Transform openHole;
        [SerializeField] private Transform closeHole;

        [Header("ANIMATION")] [SerializeField] private Transform modelTransform;
        [SerializeField] private float openDuration;
        [SerializeField] private float closeDuration;
        [SerializeField] private Vector3 animeScale;

        public void Open()
        {
            SetAnime(openDuration);

            openHole.gameObject.SetActive(true);
            closeHole.gameObject.SetActive(false);
        }

        public void Close()
        {
            SetAnime(closeDuration);

            openHole.gameObject.SetActive(false);
            closeHole.gameObject.SetActive(true);
        }

        private void SetAnime(float duration)
        {
            DOTween.Kill(modelTransform);
            modelTransform.localScale = Vector3.one;
            modelTransform.DOScale(animeScale, duration)
                .SetLoops(-1, LoopType.Yoyo);
        }
    }
}
﻿using System;
using Cinemachine;
using Core.Entity;
using Game.Item;
using Game.Portal;
using UnityEngine;
using UnityEngine.AI;

namespace Game
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private CinemachineVirtualCamera playerVirtualCamera;
        [SerializeField] private CinemachineVirtualCamera winVirtualCamera;
        [Header("GAME")] [SerializeField] private int gameLength;
        [SerializeField] private bool catAllowedAttack;
        [Header("FX")] [SerializeField] private Transform deathFX;
        [SerializeField] private Transform cheeseFX;

        public event Action<int, int> OnScoreChanged;
        public event Action<int> OnTimeTick;
        public event Action<bool> OnGameOver;

        private int _itemCount;
        private int _score;
        private int _countDownTimer;

        private Player _player;

        private void Awake()
        {
            _player = FindObjectOfType<Player>();

            InitMouseTrap();
            InitCheese();
            InitWinMouseHole();
            InitCatHitLivingEntity();
        }

        private void Start()
        {
            FreezeAllAgent(true);
            SetTimer();
        }

        #region SCORE

        private void AddScore(int score)
        {
            int oldScore = score;
            _score += score;
            OnScoreChanged?.Invoke(oldScore, _score);
        }

        #endregion

        public void StartGame()
        {
            StartTimer();
            FreezeAllAgent(false);
        }

        #region TIMER

        private void SetTimer()
        {
            _countDownTimer = gameLength;
            OnTimeTick?.Invoke(_countDownTimer);
        }

        private void StartTimer()
        {
            InvokeRepeating(nameof(Tick), 0, 1);
        }

        private void StopTimer()
        {
            CancelInvoke(nameof(Tick));
        }

        private void Tick()
        {
            OnTimeTick?.Invoke(_countDownTimer);
            if (--_countDownTimer < 0)
            {
                GameOver();
            }
        }

        #endregion

        #region ITEM

        private void InitCatHitLivingEntity()
        {
            if (!catAllowedAttack) return;
            foreach (var item in FindObjectsOfType<Cat>())
            {
                item.OnHitLivingEntity += OnHitLivingEntityEvent;
            }
        }

        private void InitMouseTrap()
        {
            foreach (var item in FindObjectsOfType<MouseTrapItem>())
            {
                item.OnPickUpItem += OnTrap;
            }
        }

        private void InitCheese()
        {
            CheeseItem[] cheeseItems = FindObjectsOfType<CheeseItem>();
            _itemCount += cheeseItems.Length;
            foreach (var item in cheeseItems)
            {
                item.OnPickUpItem += OnCheese;
            }
        }

        private void OnTrap(Item.Item item)
        {
            GameOver();
        }

        private void OnCheese(Item.Item item)
        {
            CheeseItem cheeseItem = (CheeseItem)item;
            AddScore(cheeseItem.GetScore());
            CheckCheeseCollect();

            PlayCheeseFX();
        }

        #region FX

        private void PlayCheeseFX()
        {
            cheeseFX.gameObject.SetActive(false);
            cheeseFX.transform.position = _player.GetLocation();
            cheeseFX.gameObject.SetActive(true);
        }

        #endregion

        #endregion

        #region WIN MOUSE HOLE

        private void InitWinMouseHole()
        {
            foreach (var winMouseHole in FindObjectsOfType<WinMouseHole>())
            {
                winMouseHole.OnEnterPortal += OnEnterWinPortalEvent;
            }
        }

        private void OpenWinMouseHole()
        {
            WinMouseHole winMouseHole = FindObjectOfType<WinMouseHole>();
            winMouseHole.SetHoleStatus(MouseHole.HoleStatus.Open);
            winVirtualCamera.Priority = 10;
            playerVirtualCamera.Priority = 1;
            FreezeAllAgent(true);
            StopTimer();
            Invoke(nameof(FollowPlayerDelay), 4f);
        }

        private void FollowPlayerDelay()
        {
            winVirtualCamera.Priority = 1;
            playerVirtualCamera.Priority = 10;
            FreezeAllAgent(false);
            StartTimer();
        }

        private float _oldAcceleration;

        private void FreezeAllAgent(bool freeze)
        {
            foreach (var agent in FindObjectsOfType<NavMeshAgent>())
            {
                if (freeze)
                {
                    _oldAcceleration = agent.acceleration;
                }

                agent.acceleration = freeze ? 0 : _oldAcceleration;
                agent.velocity = Vector3.zero;
            }
        }

        #endregion

        private void OnHitLivingEntityEvent(ILivingEntity livingEntity)
        {
            if (!(livingEntity is Player player)) return;
            GameOver();
        }

        private void CheckCheeseCollect()
        {
            if (--_itemCount != 0) return;
            OpenWinMouseHole();
        }

        private void OnEnterWinPortalEvent()
        {
            YouWin();
        }

        private void YouWin()
        {
            StopTimer();
            _player.Visible = false;
            OnGameOver?.Invoke(true);
        }

        private void GameOver()
        {
            StopTimer();
            _player.Visible = false;
            deathFX.transform.position = _player.GetLocation();
            deathFX.gameObject.SetActive(true);
            Invoke(nameof(GameOverDelay), 3f);
        }

        private void GameOverDelay()
        {
            OnGameOver?.Invoke(false);
        }
    }
}
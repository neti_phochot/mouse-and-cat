﻿using UnityEngine;

namespace State.Conditions
{
    [CreateAssetMenu(menuName = "PluggableAI/Condition/Cooldown", order = 0)]
    public class CooldownCondition : Condition
    {
        public override bool Test(StateController controller)
        {
            return controller.IsCooldown;
        }
    }
}
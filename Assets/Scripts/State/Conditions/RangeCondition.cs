﻿using UnityEngine;

namespace State.Conditions
{
    [CreateAssetMenu(menuName = "PluggableAI/Condition/Range", order = 0)]
    public class RangeCondition : Condition
    {
        [SerializeField] private float range;

        public override bool Test(StateController controller)
        {
            return InRange(controller);
        }

        private bool InRange(StateController controller)
        {
            return controller.Player.Visible &&
                   Vector3.Distance(controller.Npc.GetLocation(), controller.Player.GetLocation()) < range;
        }
    }
}
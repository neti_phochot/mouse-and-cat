﻿using Core.Entity;
using UnityEngine;

namespace State.Conditions
{
    [CreateAssetMenu(menuName = "PluggableAI/Condition/TargetCanSeeMe", order = 0)]
    public class TargetCanSeeMeCondition : Condition
    {
        [SerializeField] private float fov = 60;

        public override bool Test(StateController controller)
        {
            bool targetCanSeeMe = TargetCanSeeMe(controller);
            return targetCanSeeMe;
        }

        private bool TargetCanSeeMe(StateController controller)
        {
            ILivingEntity npc = controller.Npc;
            Player player = controller.Player;

            Vector3 toAgent = npc.GetLocation() - player.GetLocation();
            float lookingAble = Vector3.Angle(player.transform.forward, toAgent);
            return npc.Visible && lookingAble < fov;
        }
    }
}
﻿using Core.Entity;
using UnityEngine;

namespace State.Conditions
{
    [CreateAssetMenu(menuName = "PluggableAI/Condition/CanSeeTarget", order = 0)]
    public class CanSeeTargetCondition : Condition
    {
        public override bool Test(StateController controller)
        {
            bool canSeeTarget = CanSeeTarget(controller);
            return canSeeTarget;
        }

        private bool CanSeeTarget(StateController controller)
        {
            ILivingEntity npc = controller.Npc;
            Player player = controller.Player;
            Vector3 rayTarget = player.GetLocation() - npc.GetLocation();
            return player.Visible && Physics.Raycast(npc.GetLocation(), rayTarget, out RaycastHit raycastHit) &&
                   raycastHit.collider.CompareTag(player.gameObject.tag);
        }
    }
}
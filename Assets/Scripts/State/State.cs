﻿using UnityEngine;

namespace State
{
    [CreateAssetMenu(menuName = "PluggableAI/State", order = 0)]
    public sealed class State : ScriptableObject
    {
        public Action[] entryActions;
        public Action[] actions;
        public Action[] exitActions;
        public Transition[] transitions;

        [HideInInspector] public bool initState;

        public void UpdateState(StateController controller)
        {
            //ENTER
            CheckEntryState(controller);

            //UPDATE
            DoActions(controller, actions);

            //EXIT
            CheckTransitionToNextState(controller);
        }

        private void CheckEntryState(StateController controller)
        {
            if (initState) return;
            initState = true;
            DoActions(controller, entryActions);
        }

        private void CheckTransitionToNextState(StateController controller)
        {
            foreach (var transition in transitions)
            {
                bool decisionSucceeded = transition.Decision(controller);
                bool transitionSucceeded = controller.TransitionToNextState(
                    decisionSucceeded ? transition.trueState : transition.falseState);

                if (transitionSucceeded)
                {
                    DoActions(controller, exitActions);
                }
            }
        }

        private void DoActions(StateController controller, Action[] acts)
        {
            foreach (var action in acts)
            {
                action.Act(controller);
            }
        }
    }
}
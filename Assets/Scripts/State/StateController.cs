﻿using System;
using Core.Entity;
using Game;
using TMPro;
using UnityEngine;

namespace State
{
    public class StateController : MonoBehaviour
    {
        [SerializeField] private State currentState;
        [SerializeField] private State remainInState;

        [Header("DEBUG")] [SerializeField] private bool debug;
        [SerializeField] private TextMeshPro debugText;

        [HideInInspector] public HideManager hideManager;

        public Player Player { get; private set; }
        public ILivingEntity Npc { get; private set; }

        public bool IsCooldown { get; private set; }

        private void Awake()
        {
            hideManager = FindObjectOfType<HideManager>();
            Player = FindObjectOfType<Player>();
            Npc = GetComponent<ILivingEntity>();
            currentState.initState = false;
        }

        private void Update()
        {
            //Update State
            currentState.UpdateState(this);
        }

        public bool TransitionToNextState(State nextState)
        {
            if (nextState == remainInState) return false;
            currentState = nextState;
            currentState.initState = false;
            debugText.text =
                debug ? nextState.ToString().Replace($"({nextState.GetType()})", string.Empty) : string.Empty;
            return true;
        }

        #region COOLDOWN

        public void SetCooldown(float duration)
        {
            IsCooldown = true;
            CancelInvoke(nameof(ResetCooldown));
            Invoke(nameof(ResetCooldown), duration);
        }

        private void ResetCooldown()
        {
            IsCooldown = false;
        }

        #endregion
    }
}
﻿using UnityEngine;

namespace State
{
    [CreateAssetMenu(menuName = "PluggableAI/Action", order = 0)]
    public abstract class Action : ScriptableObject
    {
        public abstract void Act(StateController controller);
    }
}
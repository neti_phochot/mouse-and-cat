﻿using Core.Entity;
using UnityEngine;

namespace State.Actions
{
    [CreateAssetMenu(menuName = "PluggableAI/Action/Flee", order = 0)]
    public sealed class FleeAction : Action
    {
        public override void Act(StateController controller)
        {
            Flee(controller);
        }

        private void Flee(StateController controller)
        {
            ILivingEntity npc = controller.Npc;
            Vector3 destination = npc.GetLocation() - controller.Player.GetLocation() + npc.GetLocation();
            npc.GetAi().SetDestination(destination);
            
            Debug.DrawLine(npc.GetLocation(), destination, Color.green);
        }
    }
}
﻿using UnityEngine;

namespace State.Actions
{
    [CreateAssetMenu(menuName = "PluggableAI/Action/Cooldown", order = 0)]
    public sealed class CooldownAction : Action
    {
        [SerializeField] float cooldown = 3;

        public override void Act(StateController controller)
        {
            controller.SetCooldown(cooldown);
        }
    }
}
﻿using Core.Entity;
using UnityEngine;

namespace State.Actions
{
    [CreateAssetMenu(menuName = "PluggableAI/Action/CleverHide", order = 0)]
    public sealed class CleverHideAction : Action
    {
        [SerializeField] float farFactor = 5;

        public override void Act(StateController controller)
        {
            CleverHide(controller);
        }

        private void CleverHide(StateController controller)
        {
            ILivingEntity npc = controller.Npc;
            Player player = controller.Player;

            float lastDist = Mathf.Infinity;
            Vector3 chosenSpot = Vector3.zero;
            Vector3 chosenDir = Vector3.zero;
            Obstacle chosenGo = controller.hideManager.hides[0];

            for (int i = 0; i < controller.hideManager.hides.Length; i++)
            {
                Vector3 hideDir = controller.hideManager.hides[i].transform.position - player.GetLocation();
                Vector3 hidePos = controller.hideManager.hides[i].transform.position + hideDir.normalized * farFactor;

                float dist = Vector3.Distance(npc.GetLocation(), hidePos);
                if (dist < lastDist)
                {
                    chosenSpot = hidePos;
                    chosenDir = hideDir;

                    lastDist = dist;
                    chosenGo = controller.hideManager.hides[i];
                }
            }

            Collider hideCol = chosenGo.GetComponent<Collider>();
            Ray backRay = new Ray(chosenSpot, -chosenDir.normalized);
            float distance = 250.0f;
            hideCol.Raycast(backRay, out RaycastHit hitBack, distance);
            Vector3 destination = hitBack.point + chosenDir.normalized;
            npc.GetAi().SetDestination(destination);

            Debug.DrawLine(npc.GetLocation(), destination, Color.blue);
        }
    }
}
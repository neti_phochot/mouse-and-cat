﻿using Core.Entity;
using UnityEngine;

namespace State.Actions
{
    [CreateAssetMenu(menuName = "PluggableAI/Action/Pursuit", order = 0)]
    public sealed class PursuitAction : Action
    {
        public override void Act(StateController controller)
        {
            Pursuit(controller);
        }

        private void Pursuit(StateController controller)
        {
            ILivingEntity npc = controller.Npc;
            Player player = controller.Player;

            Vector3 targetDir = player.GetLocation() - npc.GetLocation();
            float playerSpeed = player.GetAi().velocity.magnitude;
            float lookAhead = targetDir.magnitude / (npc.GetAi().speed + playerSpeed);
            Vector3 destination = player.GetLocation() + player.transform.forward * lookAhead;
            npc.GetAi().SetDestination(destination);

            Debug.DrawLine(npc.GetLocation(), destination, Color.green);
        }
    }
}
﻿using Core.Entity;
using UnityEngine;

namespace State.Actions
{
    [CreateAssetMenu(menuName = "PluggableAI/Action/Wander", order = 0)]
    public sealed class WanderAction : Action
    {
        [SerializeField] float wanderRadius = 10;
        [SerializeField] float wanderDistance = 10;
        [SerializeField] float wanderJitter = 1;

        Vector3 _waderTarget = Vector3.zero;

        public override void Act(StateController controller)
        {
            Evade(controller);
        }

        private void Evade(StateController controller)
        {
            ILivingEntity npc = controller.Npc;

            _waderTarget += new Vector3(
                Random.Range(-1f, 1f) * wanderJitter,
                0,
                Random.Range(-1f, 1f) * wanderJitter);

            _waderTarget.Normalize();
            _waderTarget *= wanderRadius;

            Vector3 targetLocal = _waderTarget + new Vector3(0, 0, wanderDistance);
            Vector3 targetWorld = npc.GetAi().transform.InverseTransformVector(targetLocal);
            npc.GetAi().SetDestination(targetWorld);

            Debug.DrawLine(npc.GetLocation(), targetWorld, Color.cyan, 3);
        }
    }
}
﻿using Core.Entity;
using UnityEngine;

namespace State.Actions
{
    [CreateAssetMenu(menuName = "PluggableAI/Action/Seek", order = 0)]
    public sealed class SeekAction : Action
    {
        public override void Act(StateController controller)
        {
            Seek(controller);
        }

        private void Seek(StateController controller)
        {
            ILivingEntity npc = controller.Npc;
            Vector3 destination = controller.Player.GetLocation();
            npc.GetAi().SetDestination(destination);
            
            Debug.DrawLine(npc.GetLocation(), destination, Color.green);
        }
    }
}
﻿using UnityEngine;

namespace State.Actions
{
    [CreateAssetMenu(menuName = "PluggableAI/Action/Speed", order = 0)]
    public sealed class SpeedAction : Action
    {
        [SerializeField] private float speed;

        public override void Act(StateController controller)
        {
            controller.Npc.SetWalkSpeed(speed);
        }
    }
}
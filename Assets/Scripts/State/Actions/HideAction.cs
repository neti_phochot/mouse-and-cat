﻿using Core.Entity;
using UnityEngine;

namespace State.Actions
{
    [CreateAssetMenu(menuName = "PluggableAI/Action/Hide", order = 0)]
    public sealed class HideAction : Action
    {
        [SerializeField] float farFactor = 5;

        public override void Act(StateController controller)
        {
            Hide(controller);
        }

        private void Hide(StateController controller)
        {
            ILivingEntity npc = controller.Npc;
            Player player = controller.Player;

            float lastDist = Mathf.Infinity;
            Vector3 chosenSpot = Vector3.zero;

            for (int i = 0; i < controller.hideManager.hides.Length; i++)
            {
                Vector3 hideDir = controller.hideManager.hides[i].transform.position - player.GetLocation();
                Vector3 hidePos = controller.hideManager.hides[i].transform.position + hideDir.normalized * farFactor;

                float dist = Vector3.Distance(npc.GetLocation(), hidePos);
                if (dist < lastDist)
                {
                    chosenSpot = hidePos;
                    lastDist = dist;
                }

                npc.GetAi().SetDestination(chosenSpot);

                Debug.DrawLine(npc.GetLocation(), chosenSpot, Color.green);
            }
        }
    }
}
﻿using Core.Entity;
using UnityEngine;

namespace State.Actions
{
    [CreateAssetMenu(menuName = "PluggableAI/Action/Attack", order = 0)]
    public sealed class AttackAction : Action
    {
        [SerializeField] private float driveForce;

        public override void Act(StateController controller)
        {
            Attack(controller);
        }

        private void Attack(StateController controller)
        {
            ILivingEntity npc = controller.Npc;
            Player player = controller.Player;

            Vector3 driveDir = player.GetLocation() - npc.GetLocation();
            npc.GetAi().transform.rotation = Quaternion.LookRotation(driveDir);
            npc.AddVelocity(driveForce * driveDir);
        }
    }
}
﻿using Core.Entity;
using UnityEngine;

namespace State.Actions
{
    [CreateAssetMenu(menuName = "PluggableAI/Action/Evade", order = 0)]
    public sealed class EvadeAction : Action
    {
        public override void Act(StateController controller)
        {
            Evade(controller);
        }

        private void Evade(StateController controller)
        {
            ILivingEntity npc = controller.Npc;
            Player player = controller.Player;

            Vector3 targetDir = player.GetLocation() - npc.GetLocation();
            float playerSpeed = player.GetAi().velocity.magnitude;
            float lookAhead = targetDir.magnitude / (npc.GetAi().speed + playerSpeed);
            var fleePos = player.GetLocation() + player.transform.forward * lookAhead;
            Vector3 destination = npc.GetLocation() - fleePos + npc.GetLocation();
            npc.GetAi().SetDestination(destination);

            Debug.DrawLine(npc.GetLocation(), destination, Color.green);
        }
    }
}
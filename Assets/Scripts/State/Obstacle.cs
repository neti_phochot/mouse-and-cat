﻿using UnityEngine;

namespace State
{
    [SelectionBase]
    public class Obstacle : MonoBehaviour, ILocation
    {
        public Vector3 GetLocation() => transform.position;
        public void SetLocation(Vector3 location) => transform.position = location;
    }
}
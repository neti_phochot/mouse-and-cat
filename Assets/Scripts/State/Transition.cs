﻿using System;
using UnityEngine;

namespace State
{
    [CreateAssetMenu(menuName = "PluggableAI/Transition", order = 0)]
    public sealed class Transition : ScriptableObject
    {
        [Serializable]
        public struct ConditionData
        {
            public bool negation;
            public Condition condition;
        }

        public ConditionData[] conditions;
        public State trueState;
        public State falseState;

        public bool Decision(StateController controller)
        {
            foreach (var conditionData in conditions)
            {
                bool conditionSucceeded = conditionData.condition.Test(controller);
                conditionSucceeded = conditionData.negation ? !conditionSucceeded : conditionSucceeded;
                if (!conditionSucceeded)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
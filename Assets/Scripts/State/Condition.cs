﻿using UnityEngine;

namespace State
{
    [CreateAssetMenu(menuName = "PluggableAI/Condition", order = 0)]
    public abstract class Condition : ScriptableObject
    {
        public abstract bool Test(StateController controller);
    }
}